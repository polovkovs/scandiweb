<?php
require "product.php";

class Database extends Product 
{

	private $Host = "127.0.0.1";
	private $User = "mysql";
	private $Password = "mysql";
	private $DBname = "scandiweb";
	private $Conn;

	public function connectDB() 
	{
		$this->Conn = new mysqli($this->Host, $this->User, $this->Password, $this->DBname);

		if ($this->Conn->connect_error) {
			return "Connection failed: " . $this->Conn->connect_error;
		}
		return "<br>Connected successfully<br>";
	}

	public function displayAll() 
	{
		$sql = "SELECT * FROM product";
		$result = $this->Conn->query($sql);
		$temp = array();
		while ($row = $result->fetch_assoc()) {
			foreach ($row as $key => $value) {
				if ($key == "Type_id") {
					$sql = "SELECT name,value FROM type WHERE id LIKE $value";
					$result2 = $this->Conn->query($sql);
					$row = $result2->fetch_assoc();
					$data['Typename'] = $row['name'];
					$data['Typevalue'] = $row['value'];

				} else {
					$data[$key] = $value;
				}
			}
			array_push($temp, $data);
		}
		return $temp;
		$this->Conn->close();
	}

	public function addProduct() 
	{
		$sql = "INSERT INTO product (SKU,Name,Price,Params,Type_id) VALUES ( '" . $this->SKU . "','" . $this->Name . "','" . $this->Price . "','" . $this->Params . "','" . $this->Type . "')";
		if ($this->Conn->query($sql) === TRUE) {
			return "New record created successfully";
		} else {
			return "Error: " . $sql . "<br>" . $this->Conn->error;
		}

		$this->Conn->close();
	}

	public function findOneBySKU($sku) 
	{
		$sql = "SELECT SKU FROM product WHERE SKU LIKE '$sku'";
		$result = $this->Conn->query($sql);
		if ($result == true) {
			$row = $result->fetch_assoc();
			if (!empty($row)) {
				return true;
			}
		}
	}

	public function deleteById($id) 
	{
		$sql = "DELETE FROM product WHERE id=" . $id;
		if ($this->Conn->query($sql) === TRUE) {
			return "Record deleted successfully";
		} else {
			return "Error deleting record: " . $this->Conn->error;
		}
		$this->Conn->close();
	}

	public function getTypeIdByDesc($desc)
	{
		$sql = "SELECT id FROM type WHERE description LIKE '$desc'";
		$result = $this->Conn->query($sql);
		$row = $result->fetch_assoc();
		return $row['id'];
	}
}

?>