<?php
namespace model;

require_once "database.php";
class listModel extends \Database
{

	public $product;

	public function __construct() 
	{
		$this->connectDB();
		$this->product = $this->displayAll();
	}
	
	public function deleteSelected() 
	{
		$id = $_POST['id'];
		if (!empty($id)) {
			foreach ($id as $key => $value) {
				$this->deleteById($value);
			}
			unset($this->product);
			$this->product = $this->displayAll();
		}
	}

}
?>