<?php
namespace model;

require_once "database.php";
class addModel extends \Database
{

	public $error = [];

	public function __construct() 
	{
		$this->connectDB();

	}

	public function add()
	{
		$this->addProduct();
		unset($this->error);
	}
}
?>