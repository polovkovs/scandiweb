<?php
class Product 
{

	public $SKU;
	public $Name;
	public $Price;
	public $Params;
	public $Type;

	public function setSKU($sku) 
	{
		$this->SKU = $sku;
	}

	public function getSKU() 
	{
		return $this->SKU;
	}

	public function setName($name) 
	{
		$this->Name = $name;
	}

	public function getName() 
	{
		return $this->Name;
	}

	public function setPrice($price) 
	{
		$this->Price = $price;
	}

	public function getPrice() 
	{
		return $this->Price;
	}

	public function setParams($params) 
	{
		$this->Params = $params;
	}

	public function getParams() 
	{
		return $this->Params;
	}

	public function setType($type) 
	{
		$this->Type = $type;
	}

	public function getType() 
	{
		return $this->Type;
	}
}

?>