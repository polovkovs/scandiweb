<?php
namespace controller;

class Validator 
{	
	public function isEmpty($var)
	{
		if(empty($var)) {
			return true;
		} else {
			return false;
		}
	}

	public function isNumeric($var)
	{
		if(!preg_match('~[0-9]+~', $var)) {
			return true;
		} else {
			return false;
		}
	}

	public function existSKU($var)
	{
		if($var) {
			return true;
		} else {
			return false;
		}
	}
}
?>