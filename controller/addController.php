<?php
namespace controller;

class addController 
{

	public $model;

	public function __construct() 
	{
		$this->model = new \model\addModel();
	}

	public function listRedirect()
	{
		return new \view\listView(); 
	}

	public function errorCheck() 
	{
		$datas = $_POST;
		$datas['existsku'] = $this->model->findOneBySKU($datas['sku']);
		$datas['type_id'] = $this->model->getTypeIdByDesc($datas['tsw']);
		$validator = new validator();
		if($validator->isEmpty($datas['sku'])) {
			$this->model->error[] = "SKU field is empty";
		} elseif($validator->existSKU($datas['existsku'])) {
			$this->model->error[] = "SKU exist";
		} if($validator->isEmpty($datas['name'])){
			$this->model->error[] = "Name field is empty";
		} if($validator->isEmpty($datas['price'])){
			$this->model->error[] = "Price field is empty";
		} elseif($validator->isNumeric($datas['price'])) {
			$this->model->error[] = "You must use numbers in Price field";
		} if($validator->isEmpty($datas['tsw'])){
			$this->model->error[] = "Select type";
		} if($datas['tsw'] == "DVD-disc"){
			if($validator->isEmpty($datas['size'])) {
				$this->model->error[] = "Size field is empty";
			} elseif($validator->isNumeric($datas['size'])) {
				$this->model->error[] = "You must use numbers in Size field";
			} else {
				$this->model->setParams($datas['size']);
			}
		} elseif($datas['tsw'] == "Book") {
			if($validator->isEmpty($datas['weight'])) {
				$this->model->error[] = "Weight field is empty";
			} elseif($validator->isNumeric($datas['weight'])) {
				$this->model->error[] = "You must use numbers in Weight field";
			} else {
				$this->model->setParams($datas['weight']);
			}
		} elseif($datas['tsw'] == "Furniture") {
			if(
				$validator->isEmpty($datas['height']) ||
				$validator->isEmpty($datas['width']) ||
				$validator->isEmpty($datas['lenght'])
			) {
				$this->model->error[] = "Fill all Furniture fields";
			} elseif(
				$validator->isNumeric($datas['height']) ||
				$validator->isNumeric($datas['width']) ||
				$validator->isNumeric($datas['lenght'])
			) {
				$this->model->error[] = "You must use numbers in all Furniture fields";
			} else {
				$dim = $datas['height'] . "x" . $datas['width'] . "x" . $datas['lenght'];
				$this->model->setParams($dim);
			}
		} if($validator->isEmpty($this->model->error)) {
			$this->model->setSKU($datas['sku']);
			$this->model->setName($datas['name']);
			$this->model->setPrice($datas['price']);
			$this->model->setType($datas['type_id']);
			$this->model->add();
			return $this->listRedirect();
		}
	}
}
?>