<?php
namespace controller;

class listController 
{

	public $model;

	public function __construct() 
	{
		$this->model = new \model\listModel();

	}

	public function deleteSelected() 
	{
		$this->model->deleteSelected();
	}

	public function addRedirect()
	{
		return new \view\addView(); 
	}
}
?>
