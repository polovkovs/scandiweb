<?php
namespace view;

class addView 
{

	public $controller;
	public $header;
	public $template;

	public function __construct() 
	{
		$this->controller = new \controller\addController();
		$this->header = "template/header.php";
		$this->template = "template/addpage.php";
	}

	public function output() 
	{
		$error = $this->controller->model->error;
		require_once $this->header;
		require_once $this->template;
	}
}
?>