<?php
namespace view;

class listView 
{

	public $controller;

	public function __construct() 
	{
		$this->controller = new \controller\listController();
		$this->header = "template/header.php";
		$this->template = "template/listpage.php";
	}

	public function output() 
	{
		$data = $this->controller->model->product;
		require_once $this->header;
		require_once $this->template;
	}
}
?>