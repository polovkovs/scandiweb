$( document ).ready(function() {
    $( '.type' ).hide();
    $( 'select' ).change(function(){
    	var value = this.value;
        $( '.type' ).hide();
    	if( value == "DVD-disc" ){
    		$( '#DVD-disc' ).show();
    	}
    	else if( value == "Furniture" ){
    		$( '#Furniture' ).show();
    	}
    	else if( value == "Book" ){
    	    $( '#Book' ).show();
    	}
    });
});