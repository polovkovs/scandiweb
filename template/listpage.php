<section>
		<div class="container">
			<form action="?view=list" method="post">
			<div class="col-md-7 col-sm-6 col-12">
				<h2>Product List</h2>
			</div>
			<div class="col-md-5 col-sm-6 col-12 tr">
				<select class="" name="action" id="action">
					<option value="addRedirect">Add New</option>
					<option value="deleteSelected">Mass Delete Action</option>
				</select>
				<button type="submit" class="" id="apply">Apply</button>
			</div>
			<div class="col-md-12">
				<hr>
			</div>
			<div class="col-md-12" id="output">
<?php
if (!empty($data)) {
	foreach ($data as $key => $value) {
		echo '<div class="col-md-3 col-sm-5 col-11 tc listcom">';
		echo '<div class="col-12 tl"><input name="id[]" value="' . $value['id'] . '" type="checkbox"></div>';
		echo '<div class="col-12"><p>' . $value['SKU'] . '</p></div>';
		echo '<div class="col-12"><p>' . $value['Name'] . '</p></div>';
		echo '<div class="col-12"><p>' . $value['Price'] . '$</p></div>';
		echo '<div class="col-12"><p>';
		echo $value['Typename'].": ".$value['Params'].$value['Typevalue'] . '</p></div>';
		echo '</div>';
	}
}
?>
			</div>
			</form>
		</div>
	</section>