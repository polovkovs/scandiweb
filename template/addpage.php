<section>
		<div class="container">
			<div class="col-lg-8 col-md-6 col-sm-5 col-12">
				<h2>Product Add</h2>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-7 col-12">
				<form action="?view=add" method="post">
					<button type="submit" name="action" id="action" value="listRedirect">Return</button>
					<button type="submit" name="action" id="action" value="errorCheck">Save</button>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<hr>
			</div>
		</div>
		<div class="col-lg-8 col-md-12 col-sm-12">

				<label for="sku">SKU
					<input type="text" name="sku" value="<?php echo $_POST['sku']; ?>">
				</label><br>
				<label for="name">Name
					<input type="text" name="name" value="<?php echo $_POST['name']; ?>">
				</label><br>
				<label for="price">Price
					<input type="text" name="price" value="<?php echo $_POST['price']; ?>">
				</label><br>
				<label for="tsw">Type Switcher
					<select name="tsw">
						<option value="">Type Switcher</option>
						<option value="DVD-disc">DVD-disc</option>
						<option value="Book">Book</option>
						<option value="Furniture">Furniture</option>
					</select>
				</label><br>
				<div class="col-lg-12" id="type">
					<div class="type col-md-12" id="DVD-disc" hidden>
		    			<label for="size">Size
		    				<input type="text" name="size" value="<?php echo $_POST['size']; ?>">
		    			</label><br>
		    			<p>Example: 700 MB (Dont write MB)</p>
	    			</div>
	    			<div class="type col-md-12" id="Furniture" hidden>
	    				<label for="height">Height
	    					<input type="text" name="height" value="<?php echo $_POST['height']; ?>">
	    				</label><br>
	    				<label for="width">Width
	    					<input type="text" name="width" value="<?php echo $_POST['width']; ?>">
	    				</label><br>
	    				<label for="lenght">Lenght
	    					<input type="text" name="lenght" value="<?php echo $_POST['lenght']; ?>">
	    				</label><br>
	    				<p>Example: 24x45x15</p>
	    			</div>
	    			<div class="type col-md-12" id="Book" hidden>
		    			<label for="weight">Weight
		    				<input type="text" name="weight" value="<?php echo $_POST['weight']; ?>">
		    			</label><br>
		    			<p>Example: 2 KG (Dont write KG)</p>
	    			</div>
				</div>

				<div class="col-lg-12" id="errors">
					<?php
						if (!empty($error)) {
							foreach ($error as $key => $value) {
								echo '<p class="errors">'.$value.'</p>';
							}
						}
					?>
				</div>

			</form>
		</div>
</section>